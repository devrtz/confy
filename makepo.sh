#!/bin/bash
cd "$(dirname \"$0\")"
[ ! -d bld ] && meson bld
ninja -C bld/ confy-pot
ninja -C bld/ confy-update-po
rm -r bld
