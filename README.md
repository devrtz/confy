<img src="https://kirgroup.net/confy/logo.svg">

# Confy

Conferences schedule viewer for GNOME

Navigate Conference schedules, mark favourite talks

Built with python3, gtk3, libhandy

- [Home Page](https://confy.kirgroup.net)
- [Project Page](https://sr.ht/~fabrixxm/Confy)
- [Patches and discussions](https://lists.sr.ht/~fabrixxm/confy-dev)
- [Tickets](https://todo.sr.ht/~fabrixxm/confy)


## Packages

- [Arch Linux](https://aur.archlinux.org/packages/confy-git/) (AUR)

## Build

Requirements

- Gtk3
- libhandy 1
- Python3
- python-gobjects
- python-icalendar
- meson and ninja

### Build with Gnome Builder

Use "Clone repository..." from Open window, press "Run"

### Build from command line

    git clone https://git.sr.ht/~fabrixxm/confy
    cd confy
    meson . _build
    ninja -C _build
    ninja -C _build install

## Issues and Patches

Send issues to [~fabrixxm/confy@todo.sr.ht](https://todo.sr.ht/~fabrixxm/confy)

Send patches to [~fabrixxm/confy-dev@lists.sr.ht](https://lists.sr.ht/~fabrixxm/confy-dev)


