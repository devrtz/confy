# pages.py
#
# Copyright 2020 Fabio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import datetime
import re
import html

from gi.repository import GLib
from gi.repository import Gtk
from gi.repository import GObject
from gi.repository import GdkPixbuf
from gi.repository import Gio
from gi.repository import Handy

from .widgets import ListRowHeader, WebLinkActionRow, EventActionRow, MenuBoxRow
from .widgets import PlaceholderWidget
from .widgets import weblinkactionrowactivated
from .widgets import SimpleScalableImage, MaxSizedImage

from .config import APP_NAME, APP_ID, APP_VERSION, FMT_DAY_LIST

from . import models

def _clean_markup(text):
    """remove unsupported markup from text to use in label"""
    text = text.replace("<p>", "").replace("</p>", "\n\n")
    text = text.replace("<em>", "<i>").replace("</em>", "</i>")
    text = text.replace("<code>", "<tt>").replace("</code>", "</tt>")
    text = text.replace("<pre>", "\n<tt>").replace("</pre>", "</tt>\n")
    text = text.replace("<h1>", "<b>").replace("</h1>", "</b>")
    text = text.replace("<h2>", "<b>").replace("</h2>", "</b>")
    text = text.replace("<h3>", "<b>").replace("</h3>", "</b>")
    text = text.replace("<br>", "\n").replace("</br>", "")
    text = re.sub(r'<br */>', "\n", text)
    text = html.unescape(text)
    text = text.replace("&", "&amp;")

    # TODO: this is quite bad...
    text = re.sub(r'style="[^"]*"', "", text)

    return text.strip()


class BasePage(Gtk.ScrolledWindow):
    """Base class for all pages"""
    def __init__(self, **kwargs):
        opts = dict(
            hexpand=True,
            vexpand=True,
            width_request=200,
            kinetic_scrolling=True,
            hscrollbar_policy=Gtk.PolicyType.NEVER
        )
        if kwargs is not None:
            opts.update(kwargs)
        super().__init__(**opts)

        self.column = Handy.Clamp(
            tightening_threshold=500, # dimensione sotto la quale decide di occupare tutto lo spazio
            maximum_size=640) # dimensione massima allragandosi
        
        super().add(self.column)
        
    def add(self, widget):
        self.column.add(widget)

    def _set_pagestack(self, pageStack):
        self.pageStack = pageStack


class BaseListPage(BasePage):
    """Base class for ListBox based pages"""
    title = ""
    subtitle = ""
    
    def __init__(self, title=None, subtitle=None, **kwargs):
        super().__init__(**kwargs)
        
        title = title or self.title
        subtitle = subtitle or self.subtitle

        box = Gtk.VBox()
        box.get_style_context().add_class("main-box")
        
        if title != "" or subtitle != "":
            titlebox = Gtk.VBox(margin_left=8, margin_right=8)
            if title != "":
                label = Gtk.Label(halign=Gtk.Align.START)
                label.set_markup("<b>{}</b>".format(title.replace("&", "&amp;")))
                titlebox.pack_start(label, False, False, 0)

            if subtitle != "":
                label = Gtk.Label(halign=Gtk.Align.START)
                label.set_markup("<small>{}</small>".format(subtitle.replace("&", "&amp;")))
                label.get_style_context().add_class("dim-label")
                titlebox.pack_start(label, False, False, 0)

            box.pack_start(titlebox, False, False, 2)
            
        
        frame = Gtk.Frame(valign=Gtk.Align.START)
        
        listbox = Gtk.ListBox()
        listbox.connect('row-activated', self.on_activate)
        #listbox.get_style_context().add_class("content")
        listbox.set_header_func(self.build_list_header, None)
        
        self.listbox = listbox

        # TODO: use list model?
        self.update_list()
        
        frame.add(listbox)
        box.pack_start(frame, True, True, 2)
        
        self.add(box)
    
    def update_list(self, *args):
        self.data = list(self.get_objects())
        for w in self.listbox.get_children():
            w.destroy()
            
        lastgroup = None
        for obj in self.data:
            mn = self.build_row(obj)

            self.listbox.add(mn)
        self.listbox.show_all()
    
    def get_objects(self):
        return []
    
    def build_list_header(self, row, before, *user_data):
        if len(self.data) == 0:
            return
        obj1 = self.data[row.get_index()]
        group_txt1 = self.group_by(obj1)
        if group_txt1 is None:
            return
        if before is None:
            h = ListRowHeader(group_txt1)
            row.set_header(h)
            return
        obj2 = self.data[before.get_index()]
        group_txt2 = self.group_by(obj2)
        if group_txt2 != group_txt1:
            h = ListRowHeader(group_txt1)
            row.set_header(h)
    
    def group_by(self, obj):
        return None
    
    def build_row(self, obj):
        row = Handy.ActionRow(activatable=True, selectable=False)
        row.set_title(str(obj))
        return row
    
    def on_activate(self, listbox, actionrow):
        ...


class ConferencePage(BasePage):
    """Conference detail page"""

    def _set_logo(self, fetcher, logofile):
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(logofile)
        self.logoimage.set_from_pixbuf(pixbuf)


    def __init__(self, conf, **kwargs):
        super().__init__(**kwargs)
        
        meta = models.Meta()

        LBL_PROPS = {
            'justify': Gtk.Justification.CENTER,
            'halign' : Gtk.Align.CENTER, 
            'wrap' : False,
        }

        box = Gtk.VBox(valign=Gtk.Align.CENTER)
        
        self.logoimage = MaxSizedImage(max_width=200, max_height=200)
        f = conf.get_logo_file(cbk=self._set_logo)
        box.pack_start(self.logoimage, False, False, 16)

        label = Gtk.Label(**LBL_PROPS)
        label.set_markup("<big><b>{}</b></big>".format(_clean_markup(meta.title)))
        box.pack_start(label, False, False, 16)
        
        if meta.venue is not None:
            label = Gtk.Label(html.unescape(meta.venue), **LBL_PROPS)
            label.get_style_context().add_class("dim-label")
            box.pack_start(label, False, False, 0)
        if meta.city is not None:
            label = Gtk.Label(html.unescape(meta.city), **LBL_PROPS)
            label.get_style_context().add_class("dim-label")
            box.pack_start(label, False, False, 0)

        datebox = Gtk.VBox()
        if meta.start is not None:
            label = Gtk.Label(meta.start.strftime(FMT_DAY_LIST), **LBL_PROPS)
            datebox.pack_start(label, False, False, 8)
        if meta.end is not None and meta.end != meta.start:
            label = Gtk.Label(meta.end.strftime(FMT_DAY_LIST), **LBL_PROPS)
            datebox.pack_start(label, False, False, 8)
        box.pack_start(datebox, False, False, 16)

        
        links = conf.metadata.get('links',None)
        if links is not None:
            frame = Gtk.Frame()
            listbox = Gtk.ListBox()
            listbox.connect('row-activated', weblinkactionrowactivated)
            for l in links:
                if l['title'] != "Map":
                    row = WebLinkActionRow(html.unescape(l['title']), l['url'], l.get('type', None))
                    listbox.add(row)
            
            frame.add(listbox)
            box.pack_start(frame, False, False, 16)

        label = Gtk.Label(justify= Gtk.Justification.RIGHT, halign=Gtk.Align.END)
        last_update = datetime.datetime.fromtimestamp(meta.last_update)
        label.set_markup("<small>{}: {}</small>".format(_("cache updated"), last_update.strftime("%c")))
        label.get_style_context().add_class("dim-label")
        box.pack_start(label, False, False, 0)

        self.add(box)


class DaysPage(BaseListPage):
    """Days list page"""
    title = _("Days")
    
    def get_objects(self):
        return models.Day.all()
        
    def build_row(self, obj):
        row = Handy.ActionRow(activatable=True, selectable=False)
        row.set_title(obj.date.strftime(FMT_DAY_LIST))
        return row

    def on_activate(self, listbox, actionrow):
        idx = actionrow.get_index()
        obj = self.data[idx]
        page = EventsPage(
            title=_("Talks on {}").format(obj.date.strftime(FMT_DAY_LIST)),
            #group_by="room",
            day=obj)
        self.pageStack.pushPage(page)


class TracksPage(BaseListPage):
    """Tracks list page"""
    title = _("Tracks")
    
    def get_objects(self):
        return models.Track.all()
        
    #def group_by(self, obj):
    #    return obj.date.strftime(FMT_DAY_LIST)

    def build_row(self, obj):
        row = Handy.ActionRow(activatable=True, selectable=False)
        row.set_title(str(obj))
        row.set_subtitle(", ".join([ str(r) for r in obj.room ]))
        return row

    def on_activate(self, listbox, actionrow):
        idx = actionrow.get_index()
        obj = self.data[idx]
        subtitle = []
        if len(obj.room) == 1 and obj.room[0].name != "":
            subtitle.append(obj.room[0].name)
        
        if len(obj.date) == 1:
            subtitle.append(obj.date[0].date.strftime(FMT_DAY_LIST))

        page = EventsPage(
            title=_("{} Track").format(obj),
            subtitle=", ".join(subtitle),
            track=obj,
            group_by="day")
        self.pageStack.pushPage(page)


class RoomsPage(BaseListPage):
    """Rooms list page"""
    title = _("Rooms")
    
    def get_objects(self):
        return models.Room.all()

    def on_activate(self, listbox, actionrow):
        idx = actionrow.get_index()
        obj = self.data[idx]
        if obj.name is None:
            title = _("No Room")
        else:
            title = _("Room {}").format(obj.name)

        page = EventsPage(
            title=title,
            group_by="day",
            room=obj)
        self.pageStack.pushPage(page)


class EventsPage(BaseListPage):
    """Talks list page (called "Events" from pentabarf xml)"""
    def __init__(self, title=None, subtitle=None, group_by=None, **filters):
        self.filters = filters
        self._group_by = group_by
        super().__init__(title=title, subtitle=subtitle)
    
    def group_by(self, obj):
        if self._group_by == "day":
            return obj.date.strftime(FMT_DAY_LIST)
        if self._group_by == "track":
            return obj.track
        if self._group_by == "room":
            return obj.room
        else:
            return None
    
    def get_objects(self):
        return models.Event.filter(**self.filters)
    
    def build_row(self, obj):
        row = EventActionRow(obj)
        return row
    
    def on_activate(self, listbox, actionrow):
        idx = actionrow.get_index()
        obj = self.data[idx]
        page = EventDetailPage(obj=obj)
        self.pageStack.pushPage(page)


class SearchEventsPage(EventsPage):
    def __init__(self, searchbar):
        self.searchbar = searchbar
        self.nomatch_placeholder = PlaceholderWidget(
            text = _("No matches found"),
            icon_name = 'system-search'
        )
        self.nomatch_placeholder.show_all()
        super().__init__()

        self.listbox.set_placeholder(self.nomatch_placeholder)

    def get_objects(self):
        sb = self.searchbar
        if sb.get_search_mode() and sb.entry.get_text() != "":
            search = sb.entry.get_text().lower()
            events = models.Event.search(search)
        else:
            events = []
        return events


class EventDetailPage(BasePage):
    """Talk details page"""
    def __init__(self, obj, **kwargs):
        super().__init__(**kwargs)

        column = Handy.Clamp(
            maximum_size=1024,
            width_request=400,
            vexpand=False)

        self.obj = obj

        LBL_PROPS = {
            'justify': Gtk.Justification.FILL,
            'halign' : Gtk.Align.START, 
            'wrap' : True,
        }

        box = Gtk.VBox( margin_left=8, margin_right=8)
        
        titlebox = Gtk.VBox()

        label = Gtk.Label(**LBL_PROPS)
        label.set_markup("<big><b>{}</b></big>".format(_clean_markup(obj.title)))
        titlebox.pack_start(label, False, False, 0)

        if obj.subtitle != "":
            label = Gtk.Label(**LBL_PROPS)
            label.set_markup("{}".format(_clean_markup(obj.subtitle)))
            #label.get_style_context().add_class("dim-label")
            titlebox.pack_start(label, False, False, 0)
            
        label = Gtk.Label(**LBL_PROPS)
        label.set_markup("<small>{}</small>".format(
            ", ".join([ _clean_markup(str(p)) for p in obj.persons()])))
        label.get_style_context().add_class("dim-label")
        titlebox.pack_start(label, False, False, 0)
        
        box.pack_start(titlebox, False, False, 16)

        starbox = Gtk.HBox()
        
        titlebox = Gtk.VBox()
        label = Gtk.Label(**LBL_PROPS)
        label.set_markup("<b>{}-{}</b>".format(
            obj.start.strftime("%a %H:%M"),
            obj.end.strftime("%H:%M")))
        titlebox.pack_start(label, False, False, 0)

        if obj.room:
            label = Gtk.Label(**LBL_PROPS)
            label.set_markup(_("Room: <b>{}</b>").format(obj.room))
            titlebox.pack_start(label, False, False, 0)
        
        if obj.track:
            label = Gtk.Label(**LBL_PROPS)
            label.set_markup(_("Track: <b>{}</b>").format(obj.track))
            titlebox.pack_start(label, False, False, 0)

        starbox.pack_start(titlebox, False, True, 0)

        icon_name = "starred-symbolic" if obj.starred else "non-starred-symbolic" 
        self.toggle_btn = Gtk.Button.new_from_icon_name(icon_name, Gtk.IconSize.BUTTON)
        self.toggle_btn.connect("clicked", self.toggle_starred)
        starbox.pack_end(self.toggle_btn, False, False, 16)
        
        box.pack_start(starbox, False, False, 16)

        self.conflicts = list(obj.get_conflicts())
        if len(self.conflicts):
            cbox = Gtk.VBox()
            label = Gtk.Label(**LBL_PROPS)
            label.set_markup(_("<b>Conflicts with</b>"))
            cbox.pack_start(label, False, False, 0)
            frame = Gtk.Frame()
            listbox = Gtk.ListBox()
            listbox.connect('row-activated', self.on_conflict_activated)
            for c in self.conflicts:
                row = EventActionRow(c)
                listbox.add(row)
            frame.add(listbox)
            cbox.pack_start(frame, False, False, 0)
            box.pack_start(cbox, False, False, 16)


        # 'None' as string value should not happend but it does
        # By now just filter out, but a more in-depth look should be taken
        if obj.abstract is not None and obj.abstract != "" and obj.abstract != "None":
            label = Gtk.Label(**LBL_PROPS)
            label.set_markup(_clean_markup(obj.abstract))
            box.pack_start(label, False, False, 16)

        if obj.description is not None and obj.description != "" and obj.description != "None":
            label = Gtk.Label(**LBL_PROPS)
            label.set_markup(_clean_markup(obj.description))
            box.pack_start(label, False, False, 16)

        ## links
        self.links = list(obj.links())
        if len(self.links) > 0:
            linkbox = Gtk.VBox()
            label = Gtk.Label(**LBL_PROPS)
            label.set_markup(_("<b>Links</b>"))
            linkbox.pack_start(label, False, False, 0)

            frame = Gtk.Frame()
            listbox = Gtk.ListBox()
            listbox.connect('row-activated', weblinkactionrowactivated)

            for l in obj.links():
                row = WebLinkActionRow(l.name, l.href)
                listbox.add(row)
            
            frame.add(listbox)
            linkbox.pack_start(frame, False, False, 0)
            box.pack_start(linkbox, False, False, 16)

        self.add(box)

        # progress indicator
        Gio.Application.get_default().connect(
            "tick", lambda *_: self.queue_draw()
        )

        box.connect("draw", self._on_draw)

    def _on_draw(self, widget, cr):
        now = datetime.datetime.now()
        obj = self.obj

        w = self.get_allocated_width()

        dur = (obj.end.timestamp() - obj.start.timestamp())
        pos = (now.timestamp() - obj.start.timestamp())
        prc = min(max(pos/dur, 0.0), 1.0)
        x = w * prc

        cr.set_source_rgb(0, 0, 0)
        cr.move_to(0, 1)
        cr.line_to(x, 1)
        cr.stroke()
    
    def toggle_starred(self, button):
        self.obj.set_star(not self.obj.starred)
        icon_name = "starred-symbolic" if self.obj.starred else "non-starred-symbolic"
        button.get_image().set_from_icon_name(icon_name, Gtk.IconSize.BUTTON)

    def on_conflict_activated(self, listbox, actionrow):
        idx = actionrow.get_index()
        obj = self.conflicts[idx]
        page = EventDetailPage(obj=obj)
        self.pageStack.pushPage(page)


class StarredPage(BaseListPage):
    """Starred list page. This page is not in leaflet, but in mainstack"""
    def __init__(self, searchbar, **kwargs):
        self.empty_placeholder = PlaceholderWidget(
            _("Starred Talks"),
            _("You don't have any starred talk yet.") + "\n" + \
            _("Click on the star button to add a talk to this list " + \
                "and to be reminded when they are about to start."),
            "starred")
        self.empty_placeholder.show_all()
        self.nomatch_placeholder = PlaceholderWidget(
            text = _("No matches found"),
            icon_name = 'system-search'
        )
        self.nomatch_placeholder.show_all()

        self.searchbar = searchbar

        super().__init__(**kwargs)

        self.get_style_context().add_class("starred-page")
        self.get_style_context().add_class("clean-list")
        self.listbox.set_placeholder(self.empty_placeholder)
    
    def get_objects(self):
        return models.Event.filter(starred = True)

    def get_objects(self):
        sb = self.searchbar
        if sb.get_search_mode() and sb.entry.get_text() != "":
            self.listbox.set_placeholder(self.nomatch_placeholder)
            search = sb.entry.get_text().lower()
            events = models.Event.search(search, starred = True)
        else:
            self.listbox.set_placeholder(self.empty_placeholder)
            events = models.Event.filter(starred = True)
        return events
    
    def build_row(self, obj):
        row = EventActionRow(obj)
        return row
    
    def on_activate(self, listbox, actionrow):
        idx = actionrow.get_index()
        obj = self.data[idx]
        page = EventDetailPage(obj=obj)
        self.pageStack.pushPage(page)


class MainMenuPage(Gtk.ScrolledWindow):
    """Leaflet sidebar menu"""
    
    ITEMS = (
        ["EVENT", "emblem-system-symbolic", ConferencePage, None],
        (_("Days"), "x-office-calendar-symbolic", DaysPage, models.Day.count),
        (_("Tracks"), "format-justify-fill-symbolic", TracksPage, models.Track.count),
        (_("Rooms"),"display-projector-symbolic", RoomsPage, models.Room.count))
    
    def __init__(self, pageStack, conf):
        super().__init__(
            width_request=270,
            kinetic_scrolling=True,
            hscrollbar_policy=Gtk.PolicyType.NEVER
        )

        self.pageStack = pageStack
        self.conf = conf
        
        self.ITEMS[0][0] = conf.title
        
        self.listbox = Gtk.ListBox()
        self.update()
        self.listbox.connect('row-activated', self.on_activate)
        self.add(self.listbox)

    def update(self):
        for w in self.listbox.get_children():
            w.destroy()
        for k, item in enumerate(self.ITEMS):
            title, icon, cls, isvisible = item
            if isvisible is not None and not isvisible():
                continue
            mn = MenuBoxRow(title, icon)
            mn.item_id = k
            self.listbox.add(mn)
            self.listbox.show_all()

    def select_conference_page(self):
        """select and show ConferencePAge"""
        row = self.listbox.get_row_at_index(0)
        self.listbox.select_row(row)
        self.on_activate(self.listbox, row)

    def on_activate(self, listbox, actionrow):
        idx = actionrow.item_id
        cls = self.ITEMS[idx][2]
        # TODO: that's ugly...
        if idx==0:
            page = cls(self.conf)
        else:
            page = cls()
        self.pageStack.replaceWithPage(page)


class MapPage(Gtk.Overlay):
    def _add_image(self, fetcher, imagefile):
        image = SimpleScalableImage()
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(imagefile)
        image.set_from_pixbuf(pixbuf)
        image.show()
        self.image = image
        self.scrollarea.add(image)

    def __init__(self, conf, **kwargs):
        opts = dict(
            hexpand=True,
            vexpand=True
        )
        opts.update(kwargs)
        super().__init__(**opts)
        self.image = None


        self.scrollarea = Gtk.ScrolledWindow()
        self.add(self.scrollarea)

        toolbar = Gtk.VBox(halign=Gtk.Align.END, valign=Gtk.Align.END)
        toolbar.get_style_context().add_class("app-notification")
        toolbar.get_style_context().add_class("map-toolbar")

        self.zoomin = Gtk.ToolButton.new(
            Gtk.Image.new_from_icon_name("zoom-in-symbolic", Gtk.IconSize.SMALL_TOOLBAR),
            None)
        self.zoomout = Gtk.ToolButton.new(
            Gtk.Image.new_from_icon_name("zoom-out-symbolic", Gtk.IconSize.SMALL_TOOLBAR),
            None)

        toolbar.pack_start(self.zoomin, False, False, 8)
        toolbar.pack_start(self.zoomout, False, False, 8)

        self.add_overlay(toolbar)


        self.zoomin.connect("clicked", self.on_zoomin)
        self.zoomout.connect("clicked", self.on_zoomout)

        conf.get_map_files(cbk=self._add_image)

    def on_zoomin(self, *args):
        if self.image is not None:
            self.image.scale_by(1.1)

    def on_zoomout(self, *args):
        if self.image is not None:
            self.image.scale_by(0.9)

