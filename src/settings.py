# settings.py
#
# Copyright 2020 Fabio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gio
from gi.repository import Handy

# in secs
CACHE_NOCACHE = 0
CACHE_ONE_HOUR = 60 * 60
CACHE_ONE_DAY = 24 * CACHE_ONE_HOUR
CACHE_ONE_WEEK = 7 * CACHE_ONE_DAY

class CacheDuration:
    NOCACHE = 0
    ONEHOUR = 1
    ONEDAY = 2
    ONEWEEK = 3

    @classmethod
    def as_model(cls):
        m = Gio.ListStore.new(Handy.ValueObject)
        m.insert(0, Handy.ValueObject.new(_("No cache")))
        m.insert(1, Handy.ValueObject.new(_("One hour")))
        m.insert(2, Handy.ValueObject.new(_("One day")))
        m.insert(3, Handy.ValueObject.new(_("One week")))
        return m

    @classmethod
    def get_duration(cls, enum_value):
        return [
            CACHE_NOCACHE,
            CACHE_ONE_HOUR,
            CACHE_ONE_DAY,
            CACHE_ONE_WEEK
        ][enum_value]


class Settings(Gio.Settings):
    _instsance = None

    def __init__(self):
        """
            Init settings
        """
        Gio.Settings.__init__(self)

    @classmethod
    def instance(kls):
        return kls._instsance

    @classmethod
    def init(kls, app_id):
        """
            Initialize and return a new Settings object
        """
        settings = Gio.Settings.new(app_id)
        settings.__class__ = Settings
        kls._instsance = settings
        return settings

    def get_list_cache(self):
        return CacheDuration.get_duration(self.get_int('list-cache'))

    def get_event_cache(self):
        return CacheDuration.get_duration(self.get_int('event-cache'))


