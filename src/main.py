# main.py
#
# Copyright 2020 Fabio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Handy', '1')
gi.require_version("Notify", "0.7")
from gi.repository import Gtk, Gdk, Gio, GLib, GObject
from gi.repository import Handy
from gi.repository import Notify


from . import local

from .config import APP_NAME, APP_ID
from .window import ConfyWindow


class Application(Gtk.Application):
    @GObject.Signal
    def tick(self):
        """Event fired every minute

        Used to check for notifications to send and to update events row background
        """
        pass


    def __init__(self):
        super().__init__(application_id=APP_ID,
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        GLib.set_application_name(APP_NAME)
        GLib.set_prgname(APP_ID)

        # custom css
        css_provider = Gtk.CssProvider()
        css_provider.load_from_resource("/net/kirgroup/confy/confy.css")
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    def run(self, argv):
        self.connect("startup", self.on_startup)
        self.connect("activate", self.on_activate)
        self.connect("shutdown", self.on_shutdown)
        return super().run(argv)

    def on_startup(self, *_):
        Handy.init()
        Notify.init(APP_NAME)
        local.init(APP_ID)
        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", lambda *args: self.quit() )
        self.add_action(quit_action)
        self.set_accels_for_action("app.quit", ["<Primary>q",])
        shortcut_action = Gio.SimpleAction.new("shortcuts")
        shortcut_action.connect("activate", self.show_shortcuts)
        self.add_action(shortcut_action)
        self.set_accels_for_action("app.shortcuts", ["<Primary>question"])

        self.set_accels_for_action("win.show-preferences", ["<Primary>comma",])
        self.set_accels_for_action("win.return-to-events", ["<Primary>l",])
        self.set_accels_for_action("win.search", ["<Primary>f",])

        GLib.timeout_add_seconds(30, self._do_tick)

    def _do_tick(self):
        self.emit("tick")
        return True

    def on_activate(self, *_):
        win = self.props.active_window
        if not win:
            win = ConfyWindow(application=self)
        win.present()
        
    def on_shutdown(self, *_):
        local.close()

    def show_shortcuts(self, *_):
        b = Gtk.Builder.new_from_resource("/net/kirgroup/confy/shortcuts.ui")
        w = b.get_object("shortcuts-win")
        w.set_transient_for(self.props.active_window)
        w.props.view_name = None
        w.show()
        


def main(version):
    app = Application()
    return app.run(sys.argv)
